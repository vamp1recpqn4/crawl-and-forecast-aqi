# crawl-and-forecast-aqi
#Templated
## I. Problem

Vietnam aircrafts are labeled as 6 types:

-	Friendly (0)
-	Hostile (1)
-	Domestic (2)
-	International (3)
-	Over-flight (4)
-	Unknown (5)

The aircraft-friendly-classification program perform classification of unknown aircraft into 1 of 5 known types.

-	Input: the properties of an aircraft that was wanted to classify include:
 *	Longitude, latitude, altitude
 *	Heading, speed
 *	Mode3a (a 4-character identification of aircraft)
 *	Time on day.
-	Output: aircraft type.

## II. Methodology

The algorithm was used to solve this problem is Random Forest.
Random = randomness, Forest = the decision trees. Random Forest algorithm is a supervised learning. As its name suggests, Random Forest is a set of Decision Trees, each tree was selected an algorithm lean on random from: re-select the sample and use a part of random features from data set. RF considers each tree to be considered an independent voter. In the end of election, the most votes received from decision trees will be selected.

![alt text](images/randomForest_diagram.png "Random Forest Diagram")

## III.Implementation

### 1. The classes

####   a. DataPreprocessor
This class is data pre-processing with training data is tracked aircraft signals of the days before. They include the properties are: `track_id` (this is identified properties each different aircraft), `longitude`, `latitude`, `altitude`, `heading`, `speed`, `time_stamp `(time when radar saves a track to database), `mode3a`, `time` (time is calculated by epouch, it is period time from 1975 to now), `afs` (`aircraft_friendly_status` - label of aircraft).
The processed training data has properties are: `heading`, `longitude`, `latitude`, `altitude`, `speed`, `afs`, `mode3a`, `time_on_day`. These data will be save as a model to serve the processing to get output.




#### b.	PlotProcessing ####
This class will visualize process test and compare test result with true result.  After that, we can rate accuracy of this model. (Result and accuracy were saved in files with total time train, time test, numeric of testing and numeric of training sample).


### 2. The files ###

#### a.	File constants
This file has constant values are conventional are used in other files and classes.

#### b.	File test_generator
This file created testing data.
#### c.	File __main__
This file calls methods in other files and classes to get training and testing data. After that, it builds a model Random Forest and trains and tests with above data.

### 3.	Sequence diagram

![alt text](images/sequence_diagram.png "Sequence diagram")



### 4. Result
After many test we get the best parameters are stored under the tables below ( each parameter is tested   at least 5 times):

| number of trees |      crierion      |  raws_train | raws_test| total_time_train|total_time_test|accuracy(%)| true_count |
| ----------|:-------------:|  :-------------:| :-------------:| :-------------:|:-------------:|:-------------:|:-------------: |
| 10 |      gini     |  4306373 |1441| 00:03:16.223223|00:00:0.012001|94.240| 1358 |
| 10 |      entropy     |  4306373 |1441| 00:03:16.223223|00:00:0.012001|91.464| 1318 |
| 10 |      gini     |  4306373 |1441| 00:03:16.223223|00:00:0.012001|94.240| 1358 |
| 10 |      gini     |  4306373 |2251452| 00:03:00.336317|00:00:16.012001|96.825| 2179975 |
| 8 |      gini     |  4306373 |2251452| 00:03:33.423975|00:00:14.710826|96.64145627| 2175836 |

As we see, accuracy will be improved if increase training data but training time will be longer. After many test, the best parameters will be choice by avarage accuracy and test time by each model. So, best parameters we will use are {`10`, gini, 4306373} corresponding {`number of tree`, `criterion`, `raws_train` (number raw train)}. `Raws_train` is number tracked of 2 days 19/2/2018 and 20/2/2018. `Raws_test` is 2215436 that is number tracked of 21/2/2018.


