#name of air
TIME = 'time'
NO2 = "NO2"
CO = 'CO'
PM25 = 'PM2.5'
PM10 = 'PM10'
SO2 = 'SO2'
O3 = 'O3'

time_frequency = 48 # hours. Ex : 48 hours  = 2 days. 24 hours = 1 day
NUM_OBS = 10
OBSERS_ID = [1,
             7,
             8,
             9,
             10,
             11,
             12,
             13,
             14,
             #15
             ]

NAME_OBSERS = [ "Hoàn Kiếm",
                "Thành Công",
                "Tân Mai",
                "Kim Liên",
                "Phạm Văn Đồng",
                "Tây Mỗ",
                "Mỹ Đình",
                "Hàng Đậu",
                "Chi cục bảo vệ môi trường",
                "Minh Khai Bắc Từ Liêm",
                ]
PRE_FILENAME = "Nongdotrack_id="
AIR_TABLE = [NO2,CO,PM25, PM10, SO2, O3]
