# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 23:54:51 2018

@author: AELITa
"""

import pandas as pd
from pathlib import Path

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import time
import datetime
from sensor_constant import *
filename = "processed_data_from_sensor.csv"
data = pd.read_csv("Sensor data/Processed data/"+filename)

air_name = PM25
no2column = np.array(data[air_name])
flag = 0
#
flag = 0  # set flag is false
while flag == 0:
    try:
        num_periods = 50
        f_horizon = 1
        x_train = no2column[:(len(no2column) - (num_periods * 2))]
        x_batches = x_train.reshape(-1, num_periods, 1)
        y_train = no2column[1:(len(no2column) - (num_periods * 2)) + f_horizon]
        y_batches = y_train.reshape(-1, num_periods, 1)
        flag = 1
    except:
        
        data = data.drop(data.index[len(data) - 1])
        #
        no2column = np.array(data[air_name])


# function for test
def test_data(series, forecast, num):
    testX = no2column[-(num + forecast):][:num].reshape(-1, num_periods, 1)
    testY = no2column[-(num):].reshape(-1, num_periods, 1)
    return testX, testY
X_test, Y_test = test_data(no2column, f_horizon, num_periods * 2)
print(X_test.shape)

# training model - ( this model is default RNN)
# pre-processing for training
tf.reset_default_graph()
rnn_size = 100
learning_rate = 0.01
print("No2Colums = =", no2column.size)
X = tf.placeholder(tf.float32, [None, num_periods, 1])
Y = tf.placeholder(tf.float32, [None, num_periods, 1])

rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

output = tf.reshape(rnn_output, [-1, rnn_size])
logit = tf.layers.dense(output, 1, name="softmax")

outputs = tf.reshape(logit, [-1, num_periods, 1])
print(logit)

loss = tf.reduce_sum(tf.square(outputs - Y))

accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_step = optimizer.minimize(loss)

# start training
epochs =1000

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

for epoch in range(epochs):
    train_dict = {X: x_batches, Y: y_batches}
    sess.run(train_step, feed_dict=train_dict)
# save training model
saver = tf.train.Saver()
save_path = saver.save(sess, "models/"+filename+"_model_" + air_name + ".concentration_aqi")

# load traning and test
with tf.Session() as sess:
    # Restore variables from disk.
    saver.restore(sess, "models/"+filename+"_model_" + air_name + ".concentration_aqi")
    y_pred = sess.run(outputs, feed_dict={X: X_test})
    #y_pred = sess.run(outputs)

# compare output predicts with measurements
plt.title("Compare Weather Forecast vs Actual", fontsize=14)
plt.plot(pd.Series(np.ravel(Y_test)), "b-", markersize=10, label="Actual")
plt.plot(pd.Series(np.ravel(y_pred)), "r-", markersize=10, label="Forecast")
plt.legend(loc="upper left")
plt.xlabel("Time Periods")
plt.savefig("Results of obs/Sensors/" + air_name + ".png", dpi=500)
plt.show()
#
true_count = 0
length = len(np.ravel(y_pred))
max = 0
numb_ = 0
sum = 0
list_error = []
list_csv = []
for index in range(length):
    if (true_count > 1):
        break
    print('index=', index, ' predict = ', np.ravel(y_pred)[index], ' true = ', np.ravel(Y_test)[index],
          'distance = ', np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
    list_csv.append([np.ravel(y_pred)[index], np.ravel(Y_test)[index], np.ravel(y_pred)[index] - np.ravel(Y_test)[index]])
    list_error.append( np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
    if(abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])<3):
        numb_ +=1
    sum+=abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
    if(max< abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])):
        max = abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
print("MAx distance  = ",max)
print("AVG = ",sum/(num_periods*2))
print("Numb = ",numb_)