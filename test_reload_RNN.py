# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 21:06:10 2018

@author: AELITa
"""

import pandas as pd
from pathlib import Path

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import time
import datetime
from sensor_constant import *

data = pd.read_csv("processed_data_from_sensor.csv")
tf.reset_default_graph()
rnn_size = 100
learning_rate = 0.001
no2column = np.array(data[PM25])
num=24
num_periods =24
X_test = no2column[-(num + 1):][:num].reshape(-1, num_periods, 1)
Y_test = no2column[-(num):].reshape(-1, num_periods, 1)
testY = no2column[-(num):].reshape(-1, num_periods, 1)

X = tf.placeholder(tf.float32, [None, num_periods, 1])
Y = tf.placeholder(tf.float32, [None, num_periods, 1])

rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

output = tf.reshape(rnn_output, [-1, rnn_size])
logit = tf.layers.dense(output, 1, name="softmax")

outputs = tf.reshape(logit, [-1, num_periods, 1])

loss = tf.reduce_sum(tf.square(outputs - Y))

accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_step = optimizer.minimize(loss)

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()

with tf.Session() as sess:
    # Restore variables from disk.
    saver.restore(sess, "models/model_test_sensor_pm25.ckpt")
    #y_pred = sess.run(outputs)
    y_pred = sess.run(outputs, feed_dict={X: X_test})

# compare output predicts with measurements
plt.title("Compare Weather Forecast vs Actual", fontsize=14)
plt.plot(pd.Series(np.ravel(Y_test)), "b-", markersize=10, label="Actual")
plt.plot(pd.Series(np.ravel(y_pred)), "r-", markersize=10, label="Forecast")
plt.legend(loc="upper left")
plt.xlabel("Time Periods")
plt.show()
#
true_count = 0
length = len(np.ravel(y_pred))
max = 0
numb_ = 0
sum = 0
list_error = []
list_csv = []
for index in range(length):
    if (true_count > 1):
        break
    print('index=', index, ' predict = ', np.ravel(y_pred)[index], ' true = ', np.ravel(Y_test)[index],
          'distance = ', np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
    list_csv.append([np.ravel(y_pred)[index], np.ravel(Y_test)[index], np.ravel(y_pred)[index] - np.ravel(Y_test)[index]])
    list_error.append( np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
    if(abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])<3):
        numb_ +=1
    sum+=abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
    if(max< abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])):
        max = abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
print("MAx distance  = ",max)
print("AVG = ",sum/(num_periods*2))
print("Numb = ",numb_)