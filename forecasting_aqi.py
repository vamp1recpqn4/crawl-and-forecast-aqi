from pathlib import Path

from constants import *

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Tue Aug 14 05:12:17 2018

@author: uchihabear
"""
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# pre-processing for training and testing


class ForecastingRNNModel:

    def training_model(self, data, air_name):
        # data = pd.read_csv("Crawled_files/" + filename)

        air_column = np.array(data[air_name])
        while flag == 0:
            try:
                num_periods = 12  # half of a day. full of a day is 24 hours
                f_horizon = 1
                x_train = air_column[:(len(air_column) - (num_periods * 2))]
                x_batches = x_train.reshape(-1, num_periods, 1)
                y_train = air_column[1:(len(air_column) - (num_periods * 2)) + f_horizon]
                y_batches = y_train.reshape(-1, num_periods, 1)

                flag = 1
            except:
                print(num_periods)
                data = data.drop(data.index[0])
                data.index = range(len(data))
                #
                air_column = np.array(data[air_name])
        tf.reset_default_graph()
        if (air_column.size < 1000):
            rnn_size = 1000
        else:
            rnn_size = num_periods * 4
        learning_rate = 0.001

        X = tf.placeholder(tf.float32, [None, num_periods, 1])
        Y = tf.placeholder(tf.float32, [None, num_periods, 1])
        rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
        rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

        output = tf.reshape(rnn_output, [-1, rnn_size])
        logit = tf.layers.dense(output, 1, name="softmax")

        outputs = tf.reshape(logit, [-1, num_periods, 1])
        print(logit)

        loss = tf.reduce_sum(tf.square(outputs - Y))

        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_step = optimizer.minimize(loss)

        # start training
        epochs = air_column.size * 2

        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)

        for epoch in range(epochs):
            train_dict = {X: x_batches, Y: y_batches}
            sess.run(train_step, feed_dict=train_dict)
        # save training model
        saver = tf.train.Saver()
        save_path = saver.save(sess, "models/model_" + air_name + ".concentration_aqi")
        print("building model completed for ", air_name)

    def get_concentration_aqi_a_day(self, filename, air_name):

        data = pd.read_csv("Crawled_files/" + filename)
        tf.reset_default_graph()
        air_column = np.array(data[air_name])

        num_periods = 12
        forecast = 12 * 2;
        flag = 0  # set flag is false
        while flag == 0:
            try:
                x_train = air_column[:(len(air_column) - (num_periods * 2))]
                x_batches = x_train.reshape(-1, num_periods, 1)
                y_train = air_column[1:(len(air_column) - (num_periods * 2) + 1)]
                y_batches = y_train.reshape(-1, num_periods, 1)
                flag = 1
            except:
                print("shape of train:", x_train.shape)
                # data = data.drop(data.index[len(data) - 1])
                data = data[:(len(data) - 1)]
                #
                air_column = np.array(data[air_name])

        air_column = x_train
        # last_list_for_pred = air_column[-(num_periods * 2 + 1):][:(num_periods * 2)].reshape(-1, num_periods, 1)
        last_list_for_pred = air_column[-(num_periods * 2):].reshape(-1, num_periods, 1)

        print(last_list_for_pred.shape)
        rnn_size = num_periods * 4
        learning_rate = 0.001
        #
        X = tf.placeholder(tf.float32, [None, num_periods, 1])
        Y = tf.placeholder(tf.float32, [None, num_periods, 1])

        rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
        rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

        output = tf.reshape(rnn_output, [-1, rnn_size])
        logit = tf.layers.dense(output, 1, name="softmax")

        outputs = tf.reshape(logit, [-1, num_periods, 1])

        loss = tf.reduce_sum(tf.square(outputs - Y))

        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_step = optimizer.minimize(loss)

        # Loading model
        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)
        saver = tf.train.Saver()
        fileExist = Path("model/model_" + air_name + ".concentration_aqi")

        def training_model_for_air():
            # re-training model
            if (data.size > 1000):
                epochs = data.size * 2
            else:
                epochs = 1000
            x_train = data[:(len(data) - (num_periods * 2))]
            x_batches = x_train.reshape(-1, num_periods, 1)
            y_train = data[1:(len(data) - (num_periods * 2)) + 1]
            y_batches = y_train.reshape(-1, num_periods, 1)

            for epoch in range(epochs):
                train_dict = {X: x_batches, Y: y_batches}
                sess.run(train_step, feed_dict=train_dict)
            # save training model
            saver = tf.train.Saver()
            save_path = saver.save(sess, "models/"+filename+"_model_" + air_name + ".concentration_aqi")
            print("buidling and saving new model completed for ", air_name)

        if (fileExist.is_file()):
            print("Model existed ...")
            if (data.size % time_frequency):
                training_model_for_air()
        else:
            print("no " + air_name + " model, start training new model")
            training_model_for_air()

        # Loading model
        with tf.Session() as sess:
            # Restore variables from disk.
            saver.restore(sess, "models/" + filename + "_model_" + air_name + ".concentration_aqi")
            y_pred = sess.run(outputs, feed_dict={X: last_list_for_pred})
        return y_pred

    def build_n_test(self, filename, air_name,re_train):
        data = pd.read_csv("Crawled_files/" + filename)
        tf.reset_default_graph()
        air_column = np.array(data[air_name])
        f_horizon = 1
        num_periods = 12
        forecast = 12 * 2;
        flag = 0  # set flag is false
        while flag == 0:
            try:
                x_train = air_column[:(len(air_column) - (num_periods * 2))]
                x_batches = x_train.reshape(-1, num_periods, 1)
                y_train = air_column[1:(len(air_column) - (num_periods * 2) + 1)]
                y_batches = y_train.reshape(-1, num_periods, 1)
                flag = 1
            except:
                #print("shape of train:", x_train.shape)
                # data = data.drop(data.index[len(data) - 1])
                data = data[:(len(data) - 1)]
                #
                air_column = np.array(data[air_name])

        air_column = x_train

        # last_list_for_pred = air_column[-(num_periods * 2 + 1):][:(num_periods * 2)].reshape(-1, num_periods, 1)
        def test_data(series, forecast, num):
            testX = air_column[-(num + forecast):][:num].reshape(-1, num_periods, 1)
            testY = air_column[-(num):].reshape(-1, num_periods, 1)
            return testX, testY

        X_tesdt, Y_test = test_data(air_column, f_horizon, num_periods * 2)

        rnn_size = num_periods * 4
        learning_rate = 0.001
        #
        X = tf.placeholder(tf.float32, [None, num_periods, 1])
        Y = tf.placeholder(tf.float32, [None, num_periods, 1])

        rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
        rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

        output = tf.reshape(rnn_output, [-1, rnn_size])
        logit = tf.layers.dense(output, 1, name="softmax")

        outputs = tf.reshape(logit, [-1, num_periods, 1])

        loss = tf.reduce_sum(tf.square(outputs - Y))

        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_step = optimizer.minimize(loss)

        # Loading model
        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)
        saver = tf.train.Saver()
        model_path = "models/"+filename+"_model_" + air_name + ".concentration_aqi"
        #checkpath = tf.train.checkpoint_exists(model_path)
        def training_model_for_air():
            # re-training model
            if (data.size < 1000):
                epochs = data.size * 2
            else:
                epochs = 2000

            for epoch in range(epochs):
                train_dict = {X: x_batches, Y: y_batches}
                sess.run(train_step, feed_dict=train_dict)
            # save training model
            saver = tf.train.Saver()
            save_path = saver.save(sess, "models/"+filename+"_model_" + air_name + ".concentration_aqi")
            print("buidling and saving new model completed for ", air_name)

        if(re_train == False):
            if (tf.train.checkpoint_exists(model_path)):
                print("Model existed ...\n Loading existed model : ", model_path)
                if (data.size % time_frequency):
                    training_model_for_air()
            else:
                print("no " + air_name + " model, start training new model")
                training_model_for_air()
        else:
            print("starting training new model for "+air_name)
            training_model_for_air()

        # Loading model
        with tf.Session() as sess:
            # Restore variables from disk.
            saver.restore(sess, "models/"+filename+"_model_" + air_name + ".concentration_aqi")
            y_pred = sess.run(outputs, feed_dict={X: X_test})
        sess.close()
        return Y_test, y_pred, num_periods