# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 22:49:04 2018

@author: AELITa
"""

import pandas as pd
from pathlib import Path

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import time
import datetime
from sensor_constant import *

filename = "ESP_00979352_23mb.csv"
data = pd.read_csv("Sensor data/" + filename)
#

#d0 =datetime.datetime.strptime(data.iloc[i-1]['timestamp'],'%Y-%m-%d %H:%M:%S.%f')
def get_Data_Processed_from_sensor():
    list_processed_data = []
    HEADER_PROCESSED_DATA = ["date and time","Temperature","Humidity","PM1","PM2p5","PM10","CO"]
    sum_aqi = [0,       0,          0,      0,      0,0]
    
            # [temperate, humidity, PM1, PM2p5, PM10, CO] 
    count_aqi_in_same_hour = 0
    for index,row in data.iterrows():
        if(index ==0):
            aqi_date = data.iloc[index][timestamp].split(" ")[0] #split date
            hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
            #sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
            aqi_temp = row[Temp]
            aqi_Hum = row[Humidity]
            aqi_PM1 = row[PM1]
            aqi_PM25 = row[PM25]
            aqi_PM10 = row[PM10]
            aqi_CO = row[CO]
            count_aqi_in_same_hour = 1
        elif(index ==len(data)):
            date_time_hour = aqi_date +' '+ hour_date
            aqi_temp /= count_aqi_in_same_hour
            aqi_Hum /= count_aqi_in_same_hour
            aqi_PM1 /= count_aqi_in_same_hour
            aqi_PM25 /= count_aqi_in_same_hour
            aqi_PM10 /= count_aqi_in_same_hour
            aqi_CO /= count_aqi_in_same_hour
            list_processed_data.append([date_time_hour,aqi_temp,aqi_Hum,aqi_PM1, aqi_PM25, aqi_PM10, aqi_CO])
            aqi_date = data.iloc[index][timestamp].split(" ")[0] #split date
            hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
            #sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
            aqi_temp = row[Temp]
            aqi_Hum = row[Humidity]
            aqi_PM1 = row[PM1]
            aqi_PM25 = row[PM25]
            aqi_PM10 = row[PM10]
            aqi_CO = row[CO]
            count_aqi_in_same_hour = 1  
            break      
        else:
            if(aqi_date == data.iloc[index][timestamp].split(" ")[0]):
                if(hour_date == data.iloc[index][timestamp].split(" ")[1][0:2]):
                    aqi_temp += row[Temp]
                    aqi_Hum += row[Humidity]
                    aqi_PM1 += row[PM1]
                    aqi_PM25 += row[PM25]
                    aqi_PM10 += row[PM10]
                    aqi_CO += row[CO]
                    count_aqi_in_same_hour+=1
                    
                else:
                    #sum_aqi = sum_aqi/count_aqi_in_same_hour
                    date_time_hour = aqi_date +' '+hour_date
                    print("datetime ",date_time_hour)
                    aqi_temp /= count_aqi_in_same_hour
                    aqi_Hum /= count_aqi_in_same_hour
                    aqi_PM1 /= count_aqi_in_same_hour
                    aqi_PM25 /= count_aqi_in_same_hour
                    aqi_PM10 /= count_aqi_in_same_hour
                    aqi_CO /= count_aqi_in_same_hour
                    list_processed_data.append([date_time_hour,aqi_temp,aqi_Hum,aqi_PM1, aqi_PM25, aqi_PM10, aqi_CO])
                    aqi_date = data.iloc[index][timestamp].split(" ")[0] #split date
                    hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
                    #sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
                    aqi_temp = row[Temp]
                    aqi_Hum = row[Humidity]
                    aqi_PM1 = row[PM1]
                    aqi_PM25 = row[PM25]
                    aqi_PM10 = row[PM10]
                    aqi_CO = row[CO]
                    count_aqi_in_same_hour = 1
            else:
                #sum_aqi = sum_aqi/count_aqi_in_same_hour
                date_time_hour = aqi_date +' '+ hour_date
                aqi_temp /= count_aqi_in_same_hour
                aqi_Hum /= count_aqi_in_same_hour
                aqi_PM1 /= count_aqi_in_same_hour
                aqi_PM25 /= count_aqi_in_same_hour
                aqi_PM10 /= count_aqi_in_same_hour
                aqi_CO /= count_aqi_in_same_hour
                list_processed_data.append([date_time_hour,aqi_temp,aqi_Hum,aqi_PM1, aqi_PM25, aqi_PM10, aqi_CO])
                aqi_date = data.iloc[index][timestamp].split(" ")[0] #split date
                hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
                #sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
                aqi_temp = row[Temp]
                aqi_Hum = row[Humidity]
                aqi_PM1 = row[PM1]
                aqi_PM25 = row[PM25]
                aqi_PM10 = row[PM10]
                aqi_CO = row[CO]
                count_aqi_in_same_hour = 1     
       
    df = pd.DataFrame(data = list_processed_data,columns = HEADER_PROCESSED_DATA)
    df.to_csv("/Sensor data/Processed data/processed_data_from_sensor.csv",index = False)
    print("Completed!")

get_Data_Processed_from_sensor()

