# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 18:06:31 2018

@author: AELITa
"""

import pandas as pd
from pathlib import Path
from constants import *

# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 14 05:12:17 2018

@author: uchihabear
"""
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

filename = "NongDotrack_id=1.csv"
air_name = NO2
data = pd.read_csv(filename)
tf.reset_default_graph()
air_column = np.array(data[air_name])

num_periods = 12
forecast = 12 * 2;
last_list_for_pred = air_column[-forecast:]

rnn_size = num_periods * 4
learning_rate = 0.001
#
X = tf.placeholder(tf.float32, [None, num_periods, 1])
Y = tf.placeholder(tf.float32, [None, num_periods, 1])

rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

output = tf.reshape(rnn_output, [-1, rnn_size])
logit = tf.layers.dense(output, 1, name="softmax")

outputs = tf.reshape(logit, [-1, num_periods, 1])

loss = tf.reduce_sum(tf.square(outputs - Y))

accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_step = optimizer.minimize(loss)

# Loading model
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()
fileExist = Path("model/model_" + air_name + ".concentration_aqi")

# re-training model
print("datasize = ", data.shape[0])
if (air_.shape[0] > 1000):
    epochs = data.shape[0] * 2
else:
    epochs = 1000
print("epouch = ", epochs)
x_train = air_column[:(len(air_column) - (num_periods * 2))]
x_batches = x_train.reshape(-1, num_periods, 1)
y_train = air_column[1:(len(air_column) - (num_periods * 2) + 1)]
y_batches = y_train.reshape(-1, num_periods, 1)
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()
for epoch in range(epochs):
    train_dict = {X: x_batches, Y: y_batches}
    sess.run(train_step, feed_dict=train_dict)
# save training model
saver = tf.train.Saver()
save_path = saver.save(sess, "models/model_" + air_name + ".concentration_aqi")
print("buidling and saving new model completed for ", air_name)

if (fileExist.is_file()):
    print("Model existed ...")
    if (data.shape[0] % time_frequency == 0):
        training_model_for_air()
else:
    print("no " + air_name + " model, start training new model")
    training_model_for_air()

# Loading model
with tf.Session() as sess:
    # Restore variables from disk.
    saver.restore(sess, "models/model_" + air_name + ".concentration_aqi")
    y_pred = sess.run(outputs, feed_dict=last_list_for_pred)

##
