from bs4 import BeautifulSoup
import requests
from lxml.etree import tostring
from lxml.html.soupparser import fromstring
import pandas as pd
import numpy as np
from datetime import datetime


class Crawl_concentration_moitruongthudo_AQI:

    def getChiSo(self, url, header):
        list_TimeValues = []
        req = requests.get(url)
        mysoup = BeautifulSoup(req.content, "html.parser")
        myreq = mysoup.text
        listOfText = myreq.split('},')
        for i in range(0, len(listOfText)):
            rawforTest = listOfText[i][2:-1]
            listSplitedOfraw = rawforTest.split('","')

            keyValuePair = listSplitedOfraw[0].split(':"')
            dateTemp = keyValuePair[1]
            keyValuePair = listSplitedOfraw[1].split(':')
            if (i == len(listOfText) - 1):
                valueTemp = float(keyValuePair[1][0:-1])
            else:
                try:
                    valueTemp = float(keyValuePair[1])
                except:
                    valueTemp = np.NaN
            list_TimeValues.append([dateTemp, valueTemp])
        dataCSV = pd.DataFrame(list_TimeValues, columns=header)

        dataCSV = pd.DataFrame(list_TimeValues, columns=header)
        return dataCSV


    def getChiSoRiengSO2vaO3(self,url, header, list_dateNaN):
        list_TimeValues = []
        req = requests.get(url)
        mysoup = BeautifulSoup(req.content, "html.parser")
        myreq = mysoup.text
        listOfText = myreq.split('},')
        if (listOfText != ['[]']):
            for i in range(0, len(listOfText)):
                rawforTest = listOfText[i][2:-1]
                listSplitedOfraw = rawforTest.split('","')

                # listSplitedOfraw.remove(listSplitedOfraw[2]) # xóa phần color đi vì nó không cần thiết
                # index = 0 tương ứng với time
                # index = 1 tương ứng với value
                keyValuePair = listSplitedOfraw[0].split(':"')

                dateTemp = keyValuePair[1]

                keyValuePair = listSplitedOfraw[1].split(':')
                if (i == len(listOfText) - 1):
                    valueTemp = float(keyValuePair[1][0:-1])
                else:
                    try:
                        valueTemp = float(keyValuePair[1])
                    except:
                        valueTemp = np.NaN

                list_TimeValues.append([dateTemp, valueTemp])
            dataCSV = pd.DataFrame(list_TimeValues, columns=header)
            return dataCSV
        dataCSV = pd.DataFrame(list_dateNaN, columns=header)
        return dataCSV


    def get_concentration_Aqi(self) -> object:
        """

        :rtype: object
        """
        list_ID = [1, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        # 1 - Hoàn kiếm
        #
        list_LinkCacChiSo = ['http://moitruongthudo.vn/public/dailystat/NO2?site_id=',
                             # 'http://moitruongthudo.vn/public/dailystat/SO2?site_id=',
                             'http://moitruongthudo.vn/public/dailystat/CO?site_id=',
                             'http://moitruongthudo.vn/public/dailystat/PM2.5?site_id=',
                             'http://moitruongthudo.vn/public/dailystat/PM10?site_id=',
                             # 'http://moitruongthudo.vn/public/dailystat/03?site_id=',
                             ]
        list_Header = [["time", "NO2"],
                       # ["time","SO2"],
                       ["time", "CO"],
                       ["time", "PM2.5"],
                       ["time", "PM10"],
                       # ["time","03"],
                       ]
        list_Aqi = ['http://moitruongthudo.vn/public/dailystat/NO2?site_id=',
                    'http://moitruongthudo.vn/public/dailystat/SO2?site_id=',
                    'http://moitruongthudo.vn/public/dailystat/CO?site_id=',
                    'http://moitruongthudo.vn/public/dailystat/PM2.5?site_id=',
                    'http://moitruongthudo.vn/public/dailystat/PM10?site_id=',
                    'http://moitruongthudo.vn/public/dailystat/O3?site_id=',
                    ]
        count = 0

        for item in list_ID:
            count += 1
            count_for_CacChiSo = 0
            for Chiso in list_LinkCacChiSo:
                url_for_test = Chiso + str(item)
                index = list_LinkCacChiSo.index(Chiso)
                dataPD = self.getChiSo( url_for_test, list_Header[index])
                if (count_for_CacChiSo == 0):
                    dataLast = dataPD
                    count_for_CacChiSo += 1
                else:
                    dataLast = pd.merge(dataLast, dataPD, on=['time'])
            list_dateNaN = []

            for i in range(0, len(dataLast)):
                list_dateNaN.append([dataLast['time'][i], np.nan])
            #
            # xử lý riêng So2 và O3 vì không phải lúc nào cũng có cái để ghi
            urlTemp = 'http://moitruongthudo.vn/public/dailystat/SO2?site_id=' + str(item)
            dataRieng = self.getChiSoRiengSO2vaO3(urlTemp, ["time", "SO2"], list_dateNaN)
            dataLast = pd.merge(dataLast, dataRieng, on=['time'])
            urlTemp = 'http://moitruongthudo.vn/public/dailystat/03?site_id=' + str(item)
            dataRieng = self.getChiSoRiengSO2vaO3(urlTemp, ["time", "O3"], list_dateNaN)
            dataLast = pd.merge(dataLast, dataRieng, on=['time'])
            ##Save File
            print("hihi")
            filename = 'Crawled_files/NongDotrack_id=' + str(item) + '.csv'
            try:
                dataExists = pd.read_csv(filename, header=0)
                dataLast = pd.concat([dataLast, dataExists], ignore_index=True)
                dataLast = dataLast.drop_duplicates()  # remove duplicate
                dataLast.to_csv(filename, index=False)
            except FileNotFoundError:
                dataLast = dataLast.drop_duplicates()
                dataLast.to_csv(filename, index=False)
            if (count < 0):
                break
    print('Crawling is Completed!!!')
