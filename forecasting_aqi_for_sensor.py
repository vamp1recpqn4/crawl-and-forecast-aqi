import pandas as pd
from pathlib import Path

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import time
import datetime
from sensor_constant import *

class AQIForecastingforSensor:
    def get_processed_data_from_sensor(self,filename):
        data = pd.read_csv("Sensor data/" + filename)
        list_processed_data = []
        HEADER_PROCESSED_DATA = ["date and time", "Temperature", "Humidity", "PM1", "PM2p5", "PM10", "CO"]
        sum_aqi = [0, 0, 0, 0, 0, 0]

        # [temperate, humidity, PM1, PM2p5, PM10, CO]
        count_aqi_in_same_hour = 0
        for index, row in data.iterrows():
            if (index == 0):
                aqi_date = data.iloc[index][timestamp].split(" ")[0]  # split date
                hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
                # sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
                aqi_temp = row[Temp]
                aqi_Hum = row[Humidity]
                aqi_PM1 = row[PM1]
                aqi_PM25 = row[PM25]
                aqi_PM10 = row[PM10]
                aqi_CO = row[CO]
                count_aqi_in_same_hour = 1
            elif (index == len(data)):
                date_time_hour = aqi_date + ' ' + hour_date
                aqi_temp /= count_aqi_in_same_hour
                aqi_Hum /= count_aqi_in_same_hour
                aqi_PM1 /= count_aqi_in_same_hour
                aqi_PM25 /= count_aqi_in_same_hour
                aqi_PM10 /= count_aqi_in_same_hour
                aqi_CO /= count_aqi_in_same_hour
                list_processed_data.append([date_time_hour, aqi_temp, aqi_Hum, aqi_PM1, aqi_PM25, aqi_PM10, aqi_CO])
                aqi_date = data.iloc[index][timestamp].split(" ")[0]  # split date
                hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
                # sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
                aqi_temp = row[Temp]
                aqi_Hum = row[Humidity]
                aqi_PM1 = row[PM1]
                aqi_PM25 = row[PM25]
                aqi_PM10 = row[PM10]
                aqi_CO = row[CO]
                count_aqi_in_same_hour = 1
                break
            else:
                if (aqi_date == data.iloc[index][timestamp].split(" ")[0]):
                    if (hour_date == data.iloc[index][timestamp].split(" ")[1][0:2]):
                        aqi_temp += row[Temp]
                        aqi_Hum += row[Humidity]
                        aqi_PM1 += row[PM1]
                        aqi_PM25 += row[PM25]
                        aqi_PM10 += row[PM10]
                        aqi_CO += row[CO]
                        count_aqi_in_same_hour += 1

                    else:
                        # sum_aqi = sum_aqi/count_aqi_in_same_hour
                        date_time_hour = aqi_date + ' ' + hour_date
                        print("datetime ", date_time_hour)
                        aqi_temp /= count_aqi_in_same_hour
                        aqi_Hum /= count_aqi_in_same_hour
                        aqi_PM1 /= count_aqi_in_same_hour
                        aqi_PM25 /= count_aqi_in_same_hour
                        aqi_PM10 /= count_aqi_in_same_hour
                        aqi_CO /= count_aqi_in_same_hour
                        list_processed_data.append([date_time_hour, aqi_temp, aqi_Hum, aqi_PM1, aqi_PM25, aqi_PM10, aqi_CO])
                        aqi_date = data.iloc[index][timestamp].split(" ")[0]  # split date
                        hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
                        # sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
                        aqi_temp = row[Temp]
                        aqi_Hum = row[Humidity]
                        aqi_PM1 = row[PM1]
                        aqi_PM25 = row[PM25]
                        aqi_PM10 = row[PM10]
                        aqi_CO = row[CO]
                        count_aqi_in_same_hour = 1
                else:
                    # sum_aqi = sum_aqi/count_aqi_in_same_hour
                    date_time_hour = aqi_date + ' ' + hour_date
                    aqi_temp /= count_aqi_in_same_hour
                    aqi_Hum /= count_aqi_in_same_hour
                    aqi_PM1 /= count_aqi_in_same_hour
                    aqi_PM25 /= count_aqi_in_same_hour
                    aqi_PM10 /= count_aqi_in_same_hour
                    aqi_CO /= count_aqi_in_same_hour
                    list_processed_data.append([date_time_hour, aqi_temp, aqi_Hum, aqi_PM1, aqi_PM25, aqi_PM10, aqi_CO])
                    aqi_date = data.iloc[index][timestamp].split(" ")[0]  # split date
                    hour_date = data.iloc[index][timestamp].split(" ")[1][0:2]
                    # sum_aqi = [row[Temp], row[Humidity], row[PM1], row[PM25], row[PM10], row[CO]]
                    aqi_temp = row[Temp]
                    aqi_Hum = row[Humidity]
                    aqi_PM1 = row[PM1]
                    aqi_PM25 = row[PM25]
                    aqi_PM10 = row[PM10]
                    aqi_CO = row[CO]
                    count_aqi_in_same_hour = 1

        df = pd.DataFrame(data=list_processed_data, columns=HEADER_PROCESSED_DATA)
        df.to_csv("processed_data_from_sensor.csv", index=False)
        print("processing data is Completed!")

        #
    def build_and_save_model(self,filename):

        data = pd.read_csv("Sensor data/Processed data/"+filename)
        air_name = PM25
        no2column = np.array(data[air_name])
        flag = 0
        #
        flag = 0  # set flag is false
        while flag == 0:
            try:
                num_periods = 36
                f_horizon = 1
                x_train = no2column[:(len(no2column) - (num_periods * 2))]
                x_batches = x_train.reshape(-1, num_periods, 1)
                y_train = no2column[1:(len(no2column) - (num_periods * 2)) + f_horizon]
                y_batches = y_train.reshape(-1, num_periods, 1)
                flag = 1
            except:

                data = data.drop(data.index[len(data) - 1])
                #
                no2column = np.array(data[air_name])

        # function for test
        def test_data(series, forecast, num):
            testX = no2column[-(num + forecast):][:num].reshape(-1, num_periods, 1)
            testY = no2column[-(num):].reshape(-1, num_periods, 1)
            return testX, testY

        X_test, Y_test = test_data(no2column, f_horizon, num_periods * 2)
        print(X_test.shape)

        # training model - ( this model is default RNN)
        # pre-processing for training
        tf.reset_default_graph()
        rnn_size = RNN_SIZE
        learning_rate = LEARNING_RATE
        print("No2Colums = =", no2column.size)
        X = tf.placeholder(tf.float32, [None, num_periods, 1])
        Y = tf.placeholder(tf.float32, [None, num_periods, 1])

        rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
        rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

        output = tf.reshape(rnn_output, [-1, rnn_size])
        logit = tf.layers.dense(output, 1, name="softmax")

        outputs = tf.reshape(logit, [-1, num_periods, 1])
        print(logit)

        loss = tf.reduce_sum(tf.square(outputs - Y))

        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_step = optimizer.minimize(loss)

        # start training
        epochs = 1000

        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)

        for epoch in range(epochs):
            train_dict = {X: x_batches, Y: y_batches}
            sess.run(train_step, feed_dict=train_dict)
        # save training model
        saver = tf.train.Saver()
        save_path = saver.save(sess, "models/model_for_sensor_"+air_name+".ckpt")
        print("\nForecasting "+air_name+" for Sensor model was saved!!!\nTesting")
        plt.savefig("Results of obs/Sensors/" + air_name + ".png", dpi=500)
        # load traning and test
        with tf.Session() as sess:
            # Restore variables from disk.
            saver.restore(sess, "models/model_for_sensor_"+air_name+".ckpt")
            y_pred = sess.run(outputs, feed_dict={X: X_test})
            # y_pred = sess.run(outputs)

        # compare output predicts with measurements
        plt.title("Compare Weather Forecast vs Actual", fontsize=14)
        plt.plot(pd.Series(np.ravel(Y_test)), "b-", markersize=10, label="Actual")
        plt.plot(pd.Series(np.ravel(y_pred)), "r-", markersize=10, label="Forecast")
        plt.legend(loc="upper left")
        plt.xlabel("Time Periods")
        plt.show()
        #
        true_count = 0
        length = len(np.ravel(y_pred))
        max = 0
        numb_ = 0
        sum = 0
        list_error = []
        list_csv = []
        for index in range(length):
            if (true_count > 1):
                break
            print('index=', index, ' predict = ', np.ravel(y_pred)[index], ' true = ', np.ravel(Y_test)[index],
                  'distance = ', np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
            list_csv.append(
                [np.ravel(y_pred)[index], np.ravel(Y_test)[index], np.ravel(y_pred)[index] - np.ravel(Y_test)[index]])
            list_error.append(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
            if (abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index]) < 3):
                numb_ += 1
            sum += abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
            if (max < abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])):
                max = abs(np.ravel(y_pred)[index] - np.ravel(Y_test)[index])
        print("MAx distance  = ", max)
        print("AVG = ", sum / (num_periods * 2))
        print("Numb = ", numb_)

    #
    #timestamp_input  = np.array reshape(dataFrame).reshape(-1,num_periods ,1)
    #example : test_reload_RNN
    #output =  list forecasting aqi for num_periods hours
    def get_sensor_forecasting(self,timestamp_input,num_periods,air_name):
        tf.reset_default_graph()
        rnn_size = RNN_SIZE
        learning_rate = LEARNING_RATE



        X = tf.placeholder(tf.float32, [None, num_periods, 1])
        Y = tf.placeholder(tf.float32, [None, num_periods, 1])

        rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
        rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

        output = tf.reshape(rnn_output, [-1, rnn_size])
        logit = tf.layers.dense(output, 1, name="softmax")

        outputs = tf.reshape(logit, [-1, num_periods, 1])

        loss = tf.reduce_sum(tf.square(outputs - Y))

        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_step = optimizer.minimize(loss)

        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)
        saver = tf.train.Saver()

        with tf.Session() as sess:
            # Restore variables from disk.
            # y_pred = sess.run(outputs)
            saver.restore(sess, "models/model_for_sensor_" + air_name + ".ckpt")
            y_pred = sess.run(outputs, feed_dict={X: timestamp_input})
        return y_pred