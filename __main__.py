from constants import *
from pathlib import Path
import matplotlib.pyplot as plt
from crawl_concentration_aqi import Crawl_concentration_moitruongthudo_AQI
from forecasting_aqi import ForecastingRNNModel
import numpy as np
import pandas as pd
import tensorflow as tf
import plotly as pl

t_24_hours = []
for item in range(0,24):
    t_24_hours.append(item)
print("do it")
#get crawled data from moitruongthudo.com
#crawl = Crawl_concentration_moitruongthudo_AQI()
#crawl.get_concentration_Aqi()

re_train = False
index_of_obs=-1
# forescasting of next 24 hours plz call get_
for id in OBSERS_ID:
    index_of_obs+=1
    print("index of obs = ",index_of_obs, " id = ",id)
    filename = PRE_FILENAME+str(id)+'.csv'
    print("filename = ",filename)

    pre_dir = "Results of obs/"+NAME_OBSERS[index_of_obs]+"/"
    print("dir of OBS'results:  ",pre_dir)

    index = 0
    for air_name in AIR_TABLE:

        Y_test, Y_pred, num_periods = ForecastingRNNModel.build_n_test(None,filename,air_name,re_train)
        y_pred = []
        y_test = []
        for i in range(len(t_24_hours)):
            y_pred.append(np.ravel(Y_pred)[i])
            y_test.append(np.ravel(Y_test)[i])

        plt.plot(t_24_hours,y_test,'b-',label = "True value")
        plt.plot(t_24_hours,y_pred, "r-", label="Forescasting")
        plt.title("Foresting of "+pre_dir[15:-1])
        plt.title("forestcat")
        plt.xlabel("time (h)")
        plt.ylabel("AQI's value")
        plt.legend()

        plt.savefig(pre_dir+air_name+".png",dpi = 500)
        index+=1
        plt.close()
    print("SAVED all result to ",pre_dir[:-1])
print("\nCompleted !!!!")


#plot multi chart in a picture
''' 
fig, (ax) = plt.subplots(6, sharey = True)

ax[index].plot(t_24_hours, y_pred, "b-o", t_24_hours, y_test, "r-o")
ax[index].set(ylabel='forest casting of ' + air_name)
'''